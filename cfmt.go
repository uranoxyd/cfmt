package cfmt

import (
	"fmt"
	"reflect"
	"strings"

	"github.com/fatih/color"
)

const formatVerbs string = "vT%tbcdoOqxXUEfFgGsqp"

var TypeColorMap map[reflect.Kind]*color.Color = map[reflect.Kind]*color.Color{
	reflect.String: color.New(color.FgHiBlue),
}
var TypeColorDefault *color.Color = color.New(color.BgHiBlack)

type ValueColorFunction func(v any, c *color.Color) (handled bool)

var typeColorFunctionID int
var typeColorFunctions map[int]ValueColorFunction = make(map[int]ValueColorFunction)
var DefaultValueColorFunction ValueColorFunction = func(v any, c *color.Color) bool {
	if _, ok := v.(error); ok {
		*c = *color.New(color.FgHiRed)
		return true
	}

	switch reflect.ValueOf(v).Kind() {
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64, reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		*c = *color.New(color.FgYellow)
		return true
	case reflect.Float32, reflect.Float64:
		*c = *color.New(color.FgCyan)
		return true
	case reflect.String:
		*c = *color.New(color.FgHiBlack)
		return true
	case reflect.Bool:
		if v.(bool) {
			*c = *color.New(color.FgGreen)
			return true
		} else {
			*c = *color.New(color.FgRed)
			return true
		}
	}
	return false
}

func RegisterValueColorFunction(function ValueColorFunction) int {
	typeColorFunctions[typeColorFunctionID] = function
	typeColorFunctionID++
	return typeColorFunctionID
}

func applyColor(format string, arg any) string {
	for _, fnc := range typeColorFunctions {
		var col color.Color
		if fnc(arg, &col) {
			return col.Sprintf(format, arg)
		}
	}

	if DefaultValueColorFunction != nil {
		var col color.Color
		if DefaultValueColorFunction(arg, &col) {
			return col.Sprintf(format, arg)
		}
	}

	return fmt.Sprintf(format, arg)
}

func SPrintf(format string, args ...any) string {
	buffer := ""
	formatStart := -1
	argIndex := -1

	var chunks []struct {
		format bool
		value  string
		arg    any
	}

	appendChunk := func() {
		if len(buffer) == 0 {
			return
		}

		var arg any
		if argIndex > -1 && argIndex < len(args) {
			arg = args[argIndex]
		}

		chunks = append(chunks, struct {
			format bool
			value  string
			arg    any
		}{formatStart != -1, buffer, arg})

		buffer = ""
	}

	r := []rune(format)
	l := len(r)
	for i := 0; i < l; i++ {

		if formatStart > -1 {
			if strings.ContainsRune(formatVerbs, r[i]) {
				buffer += string(r[i])
				appendChunk()
				formatStart = -1
				continue
			}
		} else {
			if r[i] == '%' {
				argIndex++
				appendChunk()
				formatStart = i
				continue
			}
		}

		buffer += string(r[i])
	}

	appendChunk()

	newFormat := ""
	for _, chunk := range chunks {
		if chunk.format {
			newFormat += applyColor("%"+chunk.value, chunk.arg)
		} else {
			newFormat += chunk.value
		}
	}

	return newFormat
}

func Printf(format string, args ...any) {
	fmt.Print(SPrintf(format, args...))
}
