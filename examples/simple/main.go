package main

import (
	"errors"

	"gitlab.com/uranoxyd/cfmt"
)

func main() {
	cfmt.Printf("int=%d, float=%.04f, bool=%v, bool=%v, str=%s, error=%v\n", 42, 23.42, true, false, "foobar", errors.New("foobar"))
}
