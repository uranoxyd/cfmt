package main

import (
	"errors"

	"github.com/fatih/color"
	"gitlab.com/uranoxyd/cfmt"
)

func main() {
	cfmt.RegisterValueColorFunction(func(v any, c *color.Color) (handled bool) {
		if _, ok := v.(int); ok {
			*c = *color.New(color.BgBlue)
			return true
		}
		return
	})
	cfmt.Printf("int=%d, float=%.04f, bool=%v, bool=%v, str=%s, error=%v\n", 42, 23.42, true, false, "foobar", errors.New("foobar"))
}
